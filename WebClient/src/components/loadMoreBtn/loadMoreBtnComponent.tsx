import React, {FC, useEffect, useState} from 'react';
import {useInView} from 'react-intersection-observer';
import {loadChunk} from '../../api/ajaxRequests';
import useStore from '../../hooks/useStore';
import Button from '../button/buttonComponent';

import './loadMoreBtn.scss';

/* eslint-disable @typescript-eslint/no-explicit-any, @typescript-eslint/no-unused-vars*/


interface LoadMoreBtnProps {
  name?: any,
}
// anti brute-force
const DELAY = 2000;

const LoadMoreBtnComponent: FC<LoadMoreBtnProps> = (props) => {
  const {ref, inView, entry} = useInView();
  const {state, setState} = useStore();
  const [isLoadForbidden, setIsLoadForbidden] = useState(true);
  const [prevInView, setPrevInView] = useState(false);

  useEffect(() => {
    // wait for first chunk being uploaded with first query
    if (!state.spreadsheet.length) {
      setIsLoadForbidden(true);
      return;
    }
    setIsLoadForbidden(false);
  }, [state]);

  useEffect(() => {
    setPrevInView(inView);
  }, [inView]);

  useEffect(() => {
    if (inView && inView !== prevInView && !isLoadForbidden) {
      console.log('Loading more is blocked for', DELAY / 1000, 'seconds');
      setTimeout(() =>{
        console.log('Loading more is now permitted');
        setIsLoadForbidden(false);
        loadChunk(state, setState);
      }, DELAY);
      setIsLoadForbidden(true);
    }
  }, [inView, isLoadForbidden, prevInView, state, setState]);

  console.log({inView});

  return (
    <div ref={ref}>
      <Button sign='Загрузить еше' onClick={() => loadChunk(state, setState)} />
    </div>
  );
};

export default LoadMoreBtnComponent;
