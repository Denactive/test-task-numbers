import React, {FC} from 'react';

import './button.scss';


interface ButtonProps {
  className?: string,
  sign: string,
  onClick: () => void,
}


const ButtonComponent: FC<ButtonProps> = ({sign, className, onClick}) => {
  return (
    <button className={'waves-effect waves-light btn-large ' + className} onClick={onClick}>
      {sign}
    </button>
  );
};

export default ButtonComponent;
