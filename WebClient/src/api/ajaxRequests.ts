import type {IStateContext, OrderT} from '../storage/state';
import Ajax from './ajax';
import {tranformTZDate, tranformTZDateTime} from './transform';

export function loadChunk(state: IStateContext['state'], setState: IStateContext['setState']): Promise<void> {
  const chunkID = state['lastChunkId'];

  return Ajax.get({
    url: 'orders?chunkid=' + chunkID,
  }).then(({status, response}) => {
    if (status !== 200) {
      console.warn('API Error:', response.msg);
      return;
    }

    (response.data as OrderT[]).forEach(
      (el) => el.created_at = tranformTZDate(el.created_at),
    );

    // every currency update results in 'updated_at' field updating too
    // in all entries
    const isUpdated = response.data?.length > 0;

    if (!isUpdated) {
      console.log('Новых строк пока нет');
      return;
    }

    setState({
      ...state,
      lastChunkId: chunkID + 1,
      lastUpdateCurrency: tranformTZDateTime(response.data[0].updated_at),
      spreadsheet: [...state.spreadsheet, ...response.data],
    });
    return;
  });
}

export function reloadTable(state: IStateContext['state'], setState: IStateContext['setState']): Promise<void> {
  const newState = {
    ...state,
    spreadsheet: [],
    lastUpdateCurrency: '',
    lastChunkId: 0,
  };
  setState(newState);
  return loadChunk(newState, setState);
}

export function checkChanges(state: IStateContext['state'], setState: IStateContext['setState']): Promise<string> {
  return Ajax.get({
    url: 'lastupdatedtime',
  }).then(({status, response}) => {
    if (status !== 200) {
      console.warn('API Error:', response.msg);
      return '';
    }

    const lastUpdate = response.data['lastUpdateTime'];

    if (lastUpdate !== state.lastUpdateSpreadsheet) {
      setState({
        ...state,
        lastUpdateSpreadsheet: lastUpdate,
      });
    }
    return lastUpdate;
  });
}

export function loadLink(state: IStateContext['state'], setState: IStateContext['setState']): Promise<void> {
  return Ajax.get({
    url: 'link',
  }).then(({status, response}) => {
    if (status !== 200) {
      console.warn('API Error:', response.msg);
      return;
    }

    setState({
      ...state,
      link: response.data['link'],
    });
  });
}
