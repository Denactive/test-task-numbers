/* eslint-disable */
const {exec} = require('child_process');

function stdErrorExec(command) {
  return exec(command, (error, stdout, stderr) => {
    if (error) {
      console.log(`error: ${error.message}`);
      return;
    }
    if (stderr) {
      console.log(`stderr: ${stderr}`);
      return;
    }
    console.log(`${stdout}`);
  });
}

module.exports = stdErrorExec;
