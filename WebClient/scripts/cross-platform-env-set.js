/* eslint-disable */

const minimizeArgs = require('./minimizeArgs.js');

/* 
  Использование:
  Вызов скрипта с аргументами вида арг1=знач1 арг2=знач2 ...
  установит переменные окружения $арг1=знач1 $арг2=знач2 ...
  аналогично вызову $арг1=знач1 в unix и SET арг1=знач1 в win
*/ 

// https://stackoverflow.com/questions/8683895/how-do-i-determine-the-current-operating-system-with-node-js
const isWin = process.platform.startsWith('win');

function envGetter(variable) {
  return isWin ? '%' + variable + '%' : '$' + variable;
}

for (const [arg, value] of Object.entries(minimizeArgs())) {
  process.env[arg] = value;
}

// test
// const stdErrorExec = require('./stdErrorExec.js');
// for (const arg in minimizeArgs()) {
//   stdErrorExec('echo ' + arg + ' is: ' + envGetter(arg));
// }
