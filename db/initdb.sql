-- psql -U postgres # "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\PostgreSQL 14"
-- CREATE USER user_example WITH password '123456';
-- CREATE DATABASE example_db OWNER user_example;

CREATE
    OR REPLACE FUNCTION trigger_set_timestamp()
    RETURNS TRIGGER AS
$$
BEGIN
    NEW.updated_at
        = NOW();
    RETURN NEW;
END;
$$
    LANGUAGE plpgsql;



CREATE TABLE Orders
(
    id                        SERIAL                    NOT NULL PRIMARY KEY,
    table_row_index           INTEGER                   NOT NULL UNIQUE,     -- номер строки в адресе вида A1:A50
    table_row_number          INTEGER                   NOT NULL,
    order_number              INTEGER                   NOT NULL UNIQUE,
    cost_usd                  NUMERIC(12, 2)            NOT NULL,
    cost_rub                  NUMERIC(12, 2)            NOT NULL,
    delivery_date             CHAR(10)                  NOT NULL,
    created_at                TIMESTAMPTZ               NOT NULL DEFAULT NOW(),
    updated_at                TIMESTAMPTZ               NOT NULL DEFAULT NOW()
);

CREATE TRIGGER set_timestamp
    BEFORE UPDATE
    ON Orders
    FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();

CREATE
    OR REPLACE FUNCTION upsert_orders(
        arg_table_row_index INTEGER,
        arg_table_row_number INTEGER,
        arg_order_number INTEGER,
        arg_cost_usd DOUBLE PRECISION,
        arg_cost_rub DOUBLE PRECISION,
        arg_delivery_date CHAR(10)
    )
    RETURNS VOID AS
$$
DECLARE
BEGIN
    UPDATE Orders as o SET
        table_row_number = arg_table_row_number,
        order_number = arg_order_number,
        cost_usd = arg_cost_usd,
        cost_rub = arg_cost_rub,
        delivery_date = arg_delivery_date
    WHERE table_row_index = arg_table_row_index;

    IF NOT FOUND THEN

    INSERT INTO Orders(table_row_index, table_row_number, order_number, cost_rub, cost_usd, delivery_date)
    VALUES (arg_table_row_index, arg_table_row_number, arg_order_number, arg_cost_rub, arg_cost_usd, arg_delivery_date);

    END IF;
END;
$$
    LANGUAGE plpgsql;


-- TEST

-- INSERT INTO Orders(table_row_index, table_row_number, order_number, cost_rub, cost_usd, delivery_date) VALUES(2, 1, 1337, 18000, 300, '04.07.2022');
-- INSERT INTO Orders(table_row_index, table_row_number, order_number, cost_rub, cost_usd, delivery_date) VALUES(3, 2, 1338, 15000, 250, '05.07.2022');
-- INSERT INTO Orders(table_row_index, table_row_number, order_number, cost_rub, cost_usd, delivery_date) VALUES
--                                                                                                            (4, 1, 1339, 18000, 300, '04.07.2022'),
--                                                                                                            (5, 2, 1340, 15000, 250, '05.07.2022');

-- UPDATE Orders SET delivery_date = '06.07.2022' WHERE table_row_number = 2;
-- UPDATE Orders SET cost_rub = cost_usd * 2.491;
-- UPDATE Orders AS O SET
--   cost_rub = NEW_O.cost_rub,
--   cost_usd = NEW_O.cost_usd
-- from (values
--   (2, 400, 20000),
--   (3, 100, 5000)
-- ) as NEW_O(table_row_index, cost_usd, cost_rub)
-- where NEW_O.table_row_index = O.table_row_index;

-- select relname from pg_class where relkind='r' and relname !~ '^(pg_|sql_)';

-- SELECT upsert_orders(57, 1, 1342, 300::NUMERIC(12, 2), 18000::NUMERIC(12, 2), '04.07.2022'::char(10),);
