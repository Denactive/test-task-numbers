from django.contrib import admin
from django.urls import path, include
from Api import views

# V  DRF  V
urlpatterns = [
  path(r'api/orders', views.get_chunk),
  path(r'api/lastupdatedtime', views.get_last_update_time),
  path(r'api/link', views.get_ss_link),
  path('api/', include('rest_framework.urls', namespace='rest_framework'))
]
